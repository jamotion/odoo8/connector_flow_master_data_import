Master Data Import with Connector Flow
======================================

Implemented Features
--------------------

- Import directly from odoo server file
- Creation of new records
- Update of existing records
- Configurable CSV import (with definition of delimiters and many more)
- Flat file import with fixed line size and field definition by offset and length
 

Planned Features
----------------

- Automatic deletion (or disabling) of no longer transmitted entries
- Notifications for successful or failed jobs to followers  

Check the [documentation](https://bitbucket.org/jamotion/connector_flow_master_data_import/src/8.0/doc/index.rst?fileviewer=file-view-default) for further informations...
