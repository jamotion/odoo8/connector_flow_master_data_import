# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2014 initOS GmbH & Co. KG (<http://www.initos.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import os

from openerp import models, api

from openerp.addons.connector_flow.task.abstract_task import AbstractTask
import logging

_logger = logging.getLogger(__name__)


class HostRead(AbstractTask):
    """Host Configuration options:
     - source_directory:  directory on the server where files are
                            downloaded from
     - move_directory:  If present, files will be moved to this directory
                        on the server after download.
     - delete_files:  If true, files will be deleted on the server
                      after download.
    """

    def _handle_new_source(self, source_directory, file_name, move_directory):
        """open and read given file into create_file method,
           move file if move_directory is given"""
        with open(self._source_name(source_directory, file_name), "rb") as fileobj:
            data = fileobj.read()
        return self.create_file(file_name, data)

    def _source_name(self, source_directory, file_name):
        """helper to get the full name"""
        return source_directory + '/' + file_name

    def _move_file(self, source, target):
        """Moves a file on the FTP server"""
        _logger.info('Moving file %s %s' % (source, target))
        os.rename(source, target)

    def _delete_file(self, source):
        """Deletes a file from the FTP server"""
        _logger.info('Deleting file %s' % source)
        os.remove(source)

    def run(self, config=None, async=True):
        server_config = config['host']
        source_directory = server_config.get('source_directory', '')
        move_directory = server_config.get('move_directory', '')
        file_list = os.listdir(source_directory)
        downloaded_files = []
        for srcfile in file_list:
            if os.path.isfile(self._source_name(source_directory, srcfile)):
                file_id = self._handle_new_source(source_directory, srcfile, move_directory)
                self.run_successor_tasks(file_id=file_id, async=async)
                downloaded_files.append(srcfile)

        # Move/delete files only after all files have been processed.
        if server_config.get('delete_files'):
            for srcfile in downloaded_files:
                self._delete_file(self._source_name(download_directory, srcfile))
        elif move_directory:
            if not os.path.exists(move_directory):
                os.mkdir(move_directory)
            for srcfile in downloaded_files:
                self._move_file(self._source_name(source_directory, srcfile),
                                self._source_name(move_directory, srcfile))


class HostReadTask(models.Model):
    _inherit = 'impexp.task'

    @api.model
    def _get_available_tasks(self):
        return super(HostReadTask, self)._get_available_tasks() + [
            ('host_read', 'Read File from Server')]

    def host_read_class(self):
        return HostRead
