# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2004-2010 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 29.01.2016.
#

import logging
import os
import urllib2
import urlparse
from base64 import b64encode
from datetime import datetime

from openerp import models, api
from openerp.addons.connector_flow.task.abstract_task import \
    AbstractChunkReadTask
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT
from openerp.tools import DEFAULT_SERVER_TIME_FORMAT as TIME_FORMAT
from openerp.tools.safe_eval import safe_eval

EXTID_PREFIX = '__export__.'

_logger = logging.getLogger(__name__)


class MasterDataImport(AbstractChunkReadTask):
    def read_chunk(self, config=None, chunk_data=None, async=True):
        options = config.get('options', {})
        lang = options.get('language', False)
        model = config.get('model')
        model_obj = self.session.env[model]
        if not isinstance(chunk_data, list):
            chunk_data = [chunk_data]
        for line in chunk_data:
            # Read raw data from child class
            raw_data = self._map_line(line, config, options)

            external_id = False
            if 'extid' in raw_data:
                external_id = raw_data['extid'].replace(" ", "")
                record = self.session.env.ref(EXTID_PREFIX + external_id,
                                              False)

            elif 'search' in raw_data and len(raw_data['search']):
                record = model_obj.search(raw_data['search'], limit=1)
            else:
                record = False

            # now we only need the values
            raw_data = raw_data['values']
            if record:
                for odoo_field in [f for f in raw_data if
                                   isinstance(raw_data[f], dict)]:
                    source_field = raw_data[odoo_field]

                    if 'extid' in source_field:
                        odoo_field_definition = model_obj._fields[
                            odoo_field]
                        external_id = source_field['extid'] \
                            .replace(" ", "")
                        sub_entry = self.session.env.ref(
                                EXTID_PREFIX + external_id, False)
                        if sub_entry:
                            if odoo_field_definition.type == 'many2one':
                                raw_data[odoo_field] = sub_entry.id
                            else:
                                raw_data[odoo_field] = [(4, sub_entry.id,)]
                        else:
                            raise ValueError(
                                    'External Id %s not found!' % (
                                        EXTID_PREFIX + external_id))
                    elif 'search' in source_field \
                            and len(source_field['search']):
                        odoo_field_definition = model_obj._fields[
                            odoo_field]
                        sub_model = odoo_field_definition.comodel_name
                        if hasattr(odoo_field_definition, 'inverse_name'):
                            inverse_name = \
                                odoo_field_definition.inverse_name
                            source_field['search'].append(
                                    (inverse_name, '=', record.id)
                            )
                        sub_entry = self.session.env[sub_model].search(
                                source_field['search'], limit=1)
                        if sub_entry:
                            raw_data[odoo_field] = [
                                (1, sub_entry.id, source_field['values'])]
                        else:
                            if source_field['values']:
                                raw_data[odoo_field] = [
                                    (0, False, source_field['values'])]
                            else:
                                del raw_data[odoo_field]
                    elif 'values' in source_field and len(
                            source_field['values']):
                        raw_data[odoo_field] = [
                            (0, False, source_field['values'])]
                    else:
                        raw_data[odoo_field] = False
                if lang:
                    record.with_context(lang=lang).write(raw_data)
                else:
                    record.write(raw_data)
            else:
                for odoo_field in [f for f in raw_data if
                                   isinstance(raw_data[f], dict)]:
                    source_field = raw_data[odoo_field]
                    if 'extid' in source_field:
                        odoo_field_definition = model_obj._fields[
                            odoo_field]
                        tmp_external_id = source_field['extid'] \
                            .replace(" ", "")
                        sub_entry = self.session.env.ref(
                                EXTID_PREFIX + tmp_external_id, False)
                        if sub_entry:
                            raw_data[odoo_field] = sub_entry.id
                            if odoo_field_definition.type == 'many2one':
                                raw_data[odoo_field] = sub_entry.id
                            else:
                                raw_data[odoo_field] = [(4, sub_entry.id,)]
                        else:
                            raise ValueError(
                                    'External Id %s not found!' % (
                                        EXTID_PREFIX + tmp_external_id))
                    elif 'search' in source_field and len(
                            source_field['search']):
                        odoo_field_definition = model_obj._fields[
                            odoo_field]
                        sub_model = odoo_field_definition.comodel_name
                        sub_entry = self.session.env[sub_model] \
                            .search(source_field['search'], limit=1)
                        if sub_entry:
                            if odoo_field_definition.type == 'many2one':
                                raw_data[odoo_field] = sub_entry.id
                            else:
                                raw_data[odoo_field] = [(
                                    1, sub_entry.id,
                                    source_field['values']
                                )]
                        else:
                            if odoo_field_definition.type == 'many2one':
                                raise ValueError(
                                        'No entry found for search %s!' % (
                                            source_field['search']))
                            if source_field['values']:
                                raw_data[odoo_field] = [
                                    (0, False, source_field['values'])]
                            else:
                                del raw_data[odoo_field]
                    else:
                        if source_field['values']:
                            raw_data[odoo_field] = [
                                (0, False, source_field['values'])]
                        else:
                            del raw_data[odoo_field]
                if lang:
                    res = model_obj.with_context(lang=lang).create(
                            raw_data)
                else:
                    res = model_obj.create(raw_data)
                if external_id:
                    self.__create_external_id(model, external_id, res.id)

    def _map_line(self, line, config=None, options={}):
        """
        Extracts data of input line by mapping source field names to odoo
        field names (e.g. on CSV Files with headers)
        :param line: dict containing the source data
        :param model: odoo ORM name
        :param config: task configuration
        :return: dict containing the odoo fields and their values
        """
        model = config.get('model', False)
        if not model:
            raise ValueError('Config parameter "model" missing!')

        data_raw = self._map_m2x_fields(line, model, False, config, options)
        return data_raw

    def _map_list_fields(self, line, model, odoo_field, source_field, options):
        """
        this method is made for different functions:
        - Value mapping
        - Conditional values
        Sample of value mapping:
            [
                'country_id',
                (48, 'CH'),
                (48, 'Switzerland'),
                (48, 'Schweiz'),
            ]
                country_id: Odoo field name as first entry in list
                (): Tuple containing odoo value and source value
                48: Odoo value for switzerland
                'CH': Source value of file
        Sample of conditional values:
            [
                'name',
                [
                    ['|', ('firma', '==', False)],
                    ('{0} - {1}', 'Vorname', 'Nachname'),
                    ('{0}', 'Firma')
                ]
            ]
                name: Odoo field name as first entry in list
                []: List containing always three entries (condition,
                true value, false value)
                condition: typical odoo condition
                true / false: a tuple for source field value concatenation
                or a fixed value
        :param odoo_field: field name of odoo object
        :param source_field: list containing source field name and tuple of
        values
        :param line: dict containing the source data
        :param model: odoo ORM name
        :return: converted value generated for given odoo field
        """

        def _process_stack(cond, stack):
            if cond == '|' and len(stack) > 1:
                val1 = stack.pop(0)
                val2 = stack.pop(0)
                stack.insert(0, val1 or val2)
            elif cond == '&' and len(stack) > 1:
                val1 = stack.pop(0)
                val2 = stack.pop(0)
                stack.insert(0, val1 and val2)

        def _interpret_conditions(conds):
            stack = []
            for cond in conds:
                if isinstance(cond, tuple):
                    val = line.get(cond[0].decode('UTF-8'))
                    operator = cond[1].decode('UTF-8')
                    if isinstance(cond[2], basestring):
                        comparer = cond[2].decode('UTF-8')
                        if comparer[:1] == '$':
                            comparer = line.get(comparer[1:])
                        cond_string = u"'{0}' {1} '{2}'".format(val, operator,
                                                                comparer)
                    else:
                        comparer = cond[2]
                        cond_string = u"'{0}' {1} {2}".format(val, operator,
                                                              comparer)

                    stack.append(safe_eval(cond_string))
                elif isinstance(cond, basestring):
                    _process_stack(cond, stack)

            while len(stack) > 1:
                _process_stack('&', stack)

            return stack[0]

        source_field_list = source_field

        if isinstance(source_field_list[0], basestring):
            # Value mapping
            source_field = source_field_list.pop(0)
            source_value = line.get(source_field.decode('utf-8'))

            for map in source_field_list:
                if map[1] == source_value:
                    val = map[0]
                    return self._convert_value_by_field_type(model, odoo_field,
                                                             val, options)

        elif isinstance(source_field_list[0], list):
            value_mapping_list = source_field_list
            conditions = value_mapping_list.pop(0)
            true_value = value_mapping_list.pop(0)
            if isinstance(true_value, tuple):
                true_value = self._map_tuple_fields(line, model, odoo_field,
                                                    true_value, options)
            if value_mapping_list:
                false_value = value_mapping_list.pop(0)
            else:
                false_value = False
            if isinstance(false_value, tuple):
                false_value = self._map_tuple_fields(line, model, odoo_field,
                                                     false_value, options)

            result = _interpret_conditions(reversed(conditions))
            if result:
                return true_value
            else:
                return false_value

        return False

    def _map_tuple_fields(self, line, model, odoo_field, source_fields,
                          options):
        """
        this method creates a formatted string based on a tuple defined in
        config for given field.
        Sample:
            {'name': ('{0} {1}', 'firstname', 'lastname')}
                name: Odoo field name
                '{0} {1}': first entry of tuple must be the format string
                n...: the following entries are file names of source line
                where values will be extracted
        Sample with Python Code:
            {'name': (('value[:5]', '{0} {1}'), 'firstname', 'lastname')}
                name: Odoo field name
                'value[:5]': python code to evaluate, where value is the
                result of string format operation
                '{0} {1}': first entry of tuple must be the format string
                n...: the following entries are file names of source line
                where values will be extracted
        :param odoo_field: field name of odoo object
        :param source_fields: field name of dict in parameter line
        :param line: dict containing the source data
        :param model: odoo ORM name
        :return: converted value generated for given odoo field
        """
        eval_code = False
        field_parts = [t for t in source_fields]
        output_format = field_parts.pop(0)
        if isinstance(output_format, tuple) and len(output_format) == 2:
            eval_code, output_format = output_format
        values = []
        for source_field in field_parts:
            values.append(line.get(source_field.decode('UTF-8')))

        val = str(output_format).decode('utf-8').format(*values)
        if eval_code:
            val = safe_eval(eval_code, {'value': val})
        return self._convert_value_by_field_type(model, odoo_field, val,
                                                 options)

    def __create_external_id(self, model, key, id):
        module = '__export__'
        ir_model_data = self.session.env['ir.model.data']
        # Check if key already exists
        record = ir_model_data.sudo().search([
            ('model', '=', model),
            ('module', '=', module),
            ('name', '=', key)
        ])

        if record:
            record.res_id = id
            if model == 'product.product':
                record_tmpl = ir_model_data.sudo().search([
                    ('model', '=', 'product.template'),
                    ('module', '=', module),
                    ('name', '=', key.replace('product_product', 'product_template'))
                ])
                if not record_tmpl:
                    self._generate_tmpl_ext_id(id, ir_model_data, key, module)
        else:
            ir_model_data.sudo().create({
                'model': model,
                'res_id': id,
                'module': module,
                'name': key,
            })
            if model == 'product.product':
                self._generate_tmpl_ext_id(id, ir_model_data, key, module)

    def _generate_tmpl_ext_id(self, id, ir_model_data, key, module):
        prod = self.session.env['product.product'].browse(id)
        ir_model_data.sudo().create({
            'model': 'product.template',
            'res_id': prod.product_tmpl_id.id,
            'module': module,
            'name': key.replace('product_product', 'product_template'),
        })

    def __generate_xmlid(self, model, id):
        return u'{0}_{1}'.format(model.replace('.', '_'), id)

    def _map_m2x_fields(self, line, model, odoo_field, source_field, options):
        """
        this method creates entries for m2m, m2o or o2m fields
        Sample:
            {'child_ids': {'name': 'CustomerName', 'street', 'CustomerStreet'}
                child_ids: odoo field name of x2x field where values should
                be written
                name, street: odoo field name of sub model
                CustomerName, CustomerStreet: field names of source line
        :param odoo_field: field name of odoo object
        :param source_field: field name of source object
        :param line: source dict containing data
        :param model: main odoo ORM where the odoo field is located
        :return: dict containing a search and a values entry
        """
        values = {'search': [], 'values': {}}

        model_obj = self.session.env[model]

        submodel = model
        if odoo_field and model_obj._fields[odoo_field].comodel_name:
            submodel = model_obj._fields[odoo_field].comodel_name
            model_obj = self.session.env[submodel]

        # generate external id
        if 'extid' in source_field:
            source_sub_field = source_field['extid']
            if isinstance(source_sub_field, list):
                val = self._map_list_fields(line, submodel, 'extid',
                                            source_sub_field, options)
            elif isinstance(source_sub_field, tuple):
                val = self._map_tuple_fields(line, submodel, 'extid',
                                             source_sub_field, options)
            else:
                val = self._get_value_for_type(line, submodel, 'extid',
                                               source_sub_field, options)
            if val:
                if isinstance(val, basestring):
                    values['extid'] = self.__generate_xmlid(submodel,
                                                            val.strip())
                else:
                    values['extid'] = self.__generate_xmlid(submodel,
                                                            str(val).strip())

        # prepare search domain for entry
        if 'key' in source_field and not 'extid' in values:
            search_operator = options.get('search_operator', '=')
            for odoo_sub_field in source_field['key']:
                source_sub_field = source_field['key'][odoo_sub_field]
                val = self._get_value_for_type(line, submodel, odoo_sub_field,
                                               source_sub_field, options,
                                               source_sub_field)
                if val:
                    if val == 'BooleanFalse':
                        val = False
                    if isinstance(val, dict):
                        if 'extid' in val:
                            try:
                                val_record = self.session.env.ref(
                                        EXTID_PREFIX + val['extid'])
                                if val_record:
                                    val_id = val_record.id
                            except ValueError:
                                val_id = False
                            values['search'].append(
                                    (odoo_sub_field, search_operator, val_id))
                    else:
                        values['search'].append(
                                (odoo_sub_field, search_operator, val))
                else:
                    return False

        # Set default values
        if 'default' in source_field:
            for odoo_sub_field in source_field['default']:
                source_default_value = source_field['default'][odoo_sub_field]
                val = self._convert_value_by_field_type(submodel,
                                                        odoo_sub_field,
                                                        source_default_value,
                                                        options)
                if val:
                    # little hack for handling False values of boolean fields
                    if val == 'BooleanFalse':
                        val = False
                    values['values'][odoo_sub_field] = val

        if 'mapping' not in source_field:
            return values

        # put values of file into values
        for odoo_sub_field in source_field['mapping']:
            field_type = model_obj._fields[odoo_sub_field].type
            source_sub_field = source_field['mapping'][odoo_sub_field]
            if isinstance(source_sub_field, dict):
                val = self._map_m2x_fields(line, submodel, odoo_sub_field,
                                           source_sub_field, options)
            elif isinstance(source_sub_field, list):
                val = self._map_list_fields(line, submodel, odoo_sub_field,
                                            source_sub_field, options)
            elif isinstance(source_sub_field, tuple):
                val = self._map_tuple_fields(line, submodel, odoo_sub_field,
                                             source_sub_field, options)
            else:
                val = self._get_value_for_type(line, submodel, odoo_sub_field,
                                               source_sub_field, options)
            if val or field_type in ['float', 'integer'] and val == 0:
                if val == 'BooleanFalse':
                    val = False
                values['values'][odoo_sub_field] = val

        return values

    def _to_num(self, str, default=0):
        """
        Convert a string into int or float value
        :param str: input value as string
        :param default: default value if no number is extractable
        :return: float or int if conversion success, otherwise the default
        value
        """

        try:
            return int(str)
        except ValueError:
            try:
                return float(str)
            except ValueError:
                return default

    def _get_value_for_type(self, line, model, odoo_field, source_field,
                            options, default=False):
        """
        This method reads and converts the field value based on odoo field type
        :param line: source data
        :param model: Odoo ORM of given field
        :param odoo_field: odoo field name
        :param source_field: source field name of line
        :return:
        """
        if isinstance(source_field, dict):
            value = self._map_m2x_fields(line, model, odoo_field, source_field,
                                         options)
        elif isinstance(source_field, list):
            value = self._map_list_fields(line, model, odoo_field,
                                          source_field, options)
        elif isinstance(source_field, tuple):
            value = self._map_tuple_fields(line, model, odoo_field,
                                           source_field, options)
        else:
            value_raw = line.get(source_field.decode('UTF-8'), default)
            if source_field not in line:
                raise ValueError(u'Field "{0}" in file does not exist'
                                 u' maybe check for spelling mistakes!'
                                 .format(source_field))
            value = self._convert_value_by_field_type(model, odoo_field,
                                                      value_raw, options)
        if value or value == 0:
            return value
        return False

    def _convert_value_by_field_type(self, model, odoo_field, value_raw,
                                     options):
        model_obj = self.session.env[model]
        if odoo_field == 'extid':
            if isinstance(value_raw, basestring):
                value = value_raw.strip()
            else:
                value = str(value_raw).strip()
            if value and value != 'False' \
                    and value != 'false' and value != '0':
                return value
            else:
                return False

        if not odoo_field in model_obj._fields:
            raise ValueError(u'Odoo field "{0}" does not exist maybe check'
                             u' for spelling mistakes!'.format(odoo_field))

        field_type = model_obj._fields[odoo_field].type

        if field_type in ['boolean']:
            if value_raw in [True, 1, -1, 'true', 'True', 'TRUE', 'X', 'x']:
                value = True
            else:
                value = 'BooleanFalse'
        elif field_type in ['float', 'integer', 'many2one']:
            value = self._to_num(value_raw)
        elif field_type in ['date']:
            date_format = options.get('date_format', DATE_FORMAT)
            if value_raw:
                value = datetime.strptime(value_raw, date_format).date()
            else:
                value = False
        elif field_type in ['datetime']:
            date_format = options.get('date_format', DATE_FORMAT)
            time_format = options.get('time_format', TIME_FORMAT)
            datetime_format = '{0} {1}'.format(date_format, time_format)
            value = datetime.strptime(value_raw, datetime_format)
        elif field_type in ['binary']:
            base_url = options.get('base_url', False)
            if base_url:
                urlinfo = urlparse.urlparse(base_url)
                if urlinfo.netloc:
                    url = urlparse.urljoin(base_url, str(value_raw))
                else:
                    url = os.path.join(base_url, str(value_raw))
            else:
                url = str(value_raw)
            if url:
                if url.lower()[:4] in ['http', 'sftp', 'ftp:', 'ftps']:
                    urlobj = urllib2.urlopen(url)
                    value = b64encode(urlobj.read())
                else:
                    # Use file to refer to the file object
                    with open(url) as f:
                        value = b64encode(f.read())
            else:
                value = False
        elif field_type in ['one2many']:
            value = [(0, False, value_raw)]
        elif field_type in ['many2many']:
            mode = options.get('m2m_mode', 'add')
            if value_raw in [False, 'False', 'false', 0, '0']:
                value = False
            elif type(value_raw) is list:
                if mode == 'add':
                    value = [(4, v) for v in value_raw]
                else:
                    value = [(6, False, [v for v in value_raw])]
            else:
                if type(value_raw) is str:
                    tmp_val = self._to_num(value_raw)
                else:
                    tmp_val = value_raw
                if mode == 'add':
                    value = [(4, tmp_val)]
                else:
                    value = [(6, False, [tmp_val])]

        elif isinstance(value_raw, basestring):
            value = value_raw.strip()
        else:
            value = value_raw
        if value:
            return value
        elif field_type in ['float', 'integer']:
            if value == 0:
                return value
        return False


class MasterDataImportTask(models.Model):
    _inherit = 'impexp.task'

    @api.multi
    def _check_config(self):
        for record in self:
            config = record._config()
            model = config.get('model', False)
            if not model:
                raise ValueError(u'Config parameter "model" missing!')

            self._check_field_config(model, False, config)

    def _check_field_config(self, model, odoo_field, config):
        submodel = model
        model_obj = self.env[model]

        if odoo_field and model_obj._fields[odoo_field].comodel_name:
            submodel = model_obj._fields[odoo_field].comodel_name
            model_obj = self.env[submodel]

        if 'extid' in config and 'key' in config:
            raise ValueError(
                    u'Model "{0}": either use extid or key, but not both!'
                        .format(submodel))

        if 'extid' in config:
            if isinstance(config['extid'], dict):
                raise ValueError(
                        u'Model "{0}", Field "extid": Dict {{}} is'
                        u' not allowed!'.format(submodel))

        if 'key' in config:
            if not isinstance(config['key'], dict):
                raise ValueError(
                        u'Model {0}, Field "key": Search parameter has to be'
                        u' a Dict {{}}!'.format(submodel))

        if 'default' in config:
            if not isinstance(config['default'], dict):
                raise ValueError(
                        u'Model "{0}", Field "default": default has to be'
                        u' a Dict {{}}!'.format(submodel))

        if 'mapping' in config:
            if not isinstance(config['mapping'], dict):
                raise ValueError(u'Model "{0}", Field "mapping": mapping '
                                 u'definition has to be a Dict {{}}!'
                                 .format(submodel))

            for odoo_sub_field in config['mapping']:
                if odoo_sub_field not in model_obj._fields:
                    raise ValueError(
                            u"Field {0} does not exist in model {1}"
                                .format(odoo_sub_field, submodel)
                    )
                source_sub_field = config['mapping'][odoo_sub_field]
                if isinstance(source_sub_field, dict):
                    self._check_field_config(
                            submodel, odoo_sub_field, source_sub_field)

    @api.multi
    def write(self, vals):
        result = super(MasterDataImportTask, self).write(vals)
        for record in self:
            if record.task == 'master_data_import':
                record._check_config()
        return result

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        result = super(MasterDataImportTask, self).create(vals)
        if result.task == 'master_data_import':
            result._check_config()
        return result

    @api.model
    def _get_available_tasks(self):
        return super(MasterDataImportTask, self)._get_available_tasks() \
               + [('master_data_import', 'Master Data Import')]

    def master_data_import_class(self):
        return MasterDataImport
