# -*- coding: utf-8 -*-
##############################################################################
#
# OpenERP, Open Source Business Applications
# Copyright (c) 2012-TODAY OpenERP S.A. <http://openerp.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
import datetime
import urllib2
import os

from base64 import b64encode

import urlparse
from openerp import fields

import openerp
import openerp.tests

from openerp.tests import common
from openerp.exceptions import ValidationError
from tempfile import NamedTemporaryFile

_logger = logging.getLogger(__name__)


class TestMasterDataImport(common.TransactionCase):
    def test_110_convert_value_by_field_type(self):
        # Check unknown field
        with self.assertRaises(ValueError):
            self.master_data_import._convert_value_by_field_type('res.partner',
                                                                 'unknown_field',
                                                                 'Value', {})

        # Check float and int values and 0 possible
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'color', 0, {})
        self.assertEqual(result, 0)
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'color', 0.0, {})
        self.assertEqual(result, 0)

        # Check external id values
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'extid', ' Test ExtId Value ', {})
        self.assertTrue(type(result) is str)
        self.assertEqual(result, 'Test ExtId Value')

        # Check CHAR values
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'name', 'Test Char Value', {})
        self.assertTrue(type(result) is str)
        self.assertEqual(result, 'Test Char Value')

        # Check BOOLEAN values
        for val in [True, 'true', 'True', 1, -1, 'X', 'x']:
            result = self.master_data_import._convert_value_by_field_type(
                    'res.partner', 'customer', val, {})
            self.assertTrue(type(result) is bool)
            self.assertEqual(result, True)
        for val in [False, 'false', 'False', 0, 'foo']:
            result = self.master_data_import._convert_value_by_field_type(
                    'res.partner', 'customer', val, {})
            self.assertTrue(type(result) is str)
            self.assertEqual(result, 'BooleanFalse')

        # Check DATE value in odoo format
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'date', '2015-12-31', {})
        self.assertTrue(type(result) is datetime.date)
        self.assertEqual(result, datetime.date(2015, 12, 31))
        # Check DATE value in custom format
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'date', '31.12.2015',
                self.master_data_import_options)
        self.assertTrue(type(result) is datetime.date)
        self.assertEqual(result, datetime.date(2015, 12, 31))

        # Check DATETIME value in odoo format
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'write_date',
                '2015-12-31 13:59:59', {})
        self.assertTrue(type(result) is datetime.datetime)
        self.assertEqual(result, datetime.datetime(2015, 12, 31, 13, 59, 59))
        # Check DATETIME value in custom format
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'write_date', '31.12.2015 13:59',
                self.master_data_import_options)
        self.assertTrue(type(result) is datetime.datetime)
        self.assertEqual(result, datetime.datetime(2015, 12, 31, 13, 59, 0))

        # Check Image field from URL and File
        ## Check by URL
        url = 'https://jamotion.ch/logo.png'
        url_obj = urllib2.urlopen(url)
        bin_data = url_obj.read()
        test_val = b64encode(bin_data)
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'image', url, {})
        self.assertEqual(result, test_val)

        # Check by direct file read
        f = NamedTemporaryFile()
        f.write(bin_data)
        f.flush()
        f.seek(0)
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'image', f.name, {})
        self.assertEqual(result, test_val)
        f.close()

        # Check by direct file read with base_url parameter
        options = self.master_data_import_options
        f = NamedTemporaryFile()
        f.write(bin_data)
        f.flush()
        f.seek(0)
        path, filename = os.path.split(f.name)
        options.update({'base_url': path})
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'image', filename, options)
        self.assertEqual(result, test_val)
        f.close()

        ## Check by URL with base_url parameter
        base_url = 'https://jamotion.ch'
        file_path = 'logo.png'
        options.update({'base_url': base_url})
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'image', file_path, options)
        self.assertEqual(result, test_val)

        # Check MANY2ONE value
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'commercial_partner_id', '5', {})
        self.assertTrue(type(result) is int)
        self.assertEqual(result, 5)
        # Check MANY2ONE with no value
        for val in [False, 'false', 'False', 0, '0', '']:
            result = self.master_data_import._convert_value_by_field_type(
                    'res.partner', 'commercial_partner_id', val,
                    {})
            self.assertTrue(type(result) is bool)
            self.assertEqual(result, False)

        # Check ONE2MANY value
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'child_ids',
                {'name': 'Test Contact'}, {})
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(0, False, {'name': 'Test Contact'})])

        # Check MANY2MANY value with list of ids in add mode
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'message_follower_ids', [1, 2, 3],
                {})
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(4, 1), (4, 2), (4, 3)])
        # Check MANY2MANY value with list of ids ind replace mode
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'message_follower_ids', [1, 2, 3],
                self.master_data_import_options)
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(6, False, [1, 2, 3])])

        # Check MANY2MANY value with string containing id in add mode
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'message_follower_ids', '1', {})
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(4, 1)])
        # Check MANY2MANY value with string containing id in replace mode
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'message_follower_ids', '1',
                self.master_data_import_options)
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(6, False, [1])])

        # Check MANY2MANY value with integer containing id in add mode
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'message_follower_ids', 1, {})
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(4, 1)])
        # Check MANY2MANY value with integer containing id in replace mode
        result = self.master_data_import._convert_value_by_field_type(
                'res.partner', 'message_follower_ids', 1,
                self.master_data_import_options)
        self.assertTrue(type(result) is list)
        self.assertListEqual(result, [(6, False, [1])])

    def test_120_map_tuple_fields(self):
        line = {
            'field1': 'Value 1',
            'field2': 'Value 2'
        }
        # Simple string format check
        tuple_value = ('{0} - {1}', 'field1', 'field2')
        result = self.master_data_import._map_tuple_fields(line, 'res.partner',
                                                           'name', tuple_value,
                                                           {})
        self.assertEqual(result, 'Value 1 - Value 2')

        # Simple string format check with invalid field name
        tuple_value = ('{0} - {1}: {2}', 'field1', 'field2', 'field3')
        result = self.master_data_import._map_tuple_fields(line, 'res.partner',
                                                           'name', tuple_value,
                                                           {})
        self.assertEqual(result, 'Value 1 - Value 2: None')

        # Python Code check
        tuple_value = (('value[2:13]', '{0} - {1}'), 'field1', 'field2')
        result = self.master_data_import._map_tuple_fields(line, 'res.partner',
                                                           'name', tuple_value,
                                                           {})
        self.assertEqual(result, 'lue 1 - Val')

    def test_130_map_list_fields(self):
        line = {
            'language1': 'German',
            'language2': 'English',
            'language3': 'Deutsch',
            'language4': 'Deutsch',
            'is_customer': 'Ja',
            'is_supplier': 'Nein',
        }

        # Simple value mapping check
        list_value = ['language1', ('en_US', 'English'), ('de_DE', 'German')]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'lang', list_value,
                                                          {})
        self.assertEqual(result, 'de_DE')
        list_value = ['language2', ('en_US', 'English'), ('de_DE', 'German')]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'lang', list_value,
                                                          {})
        self.assertEqual(result, 'en_US')
        # Value mapping check with unknown value
        list_value = ['language3', ('en_US', 'English'), ('de_DE', 'German')]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'lang', list_value,
                                                          {})
        self.assertEqual(result, False)

        # Check simple conditions
        condition_value = [[('is_customer', '==', 'Ja')], True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, True)
        condition_value = [[('is_supplier', '==', 'Ja')], True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'supplier',
                                                          condition_value, {})
        self.assertEqual(result, False)
        # Check and conditions
        condition_value = [
            [('is_customer', '==', 'Ja'), ('is_supplier', '==', 'Nein')], True,
            False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, True)
        # Check negative and conditions
        condition_value = [
            [('is_customer', '==', 'Ja'), ('is_supplier', '==', 'Ja')], True,
            False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, False)
        # Check or conditions
        condition_value = [
            ['|', ('is_customer', '==', 'Ja'), ('is_supplier', '==', 'Ja')],
            True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, True)
        # Check negative or conditions
        condition_value = [
            ['|', ('is_customer', '==', 'Nein'), ('is_supplier', '==', 'Ja')],
            True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, False)
        # Check complex conditions
        # If is_customer==Nein and (is_supplier=Nein or language1==German)
        condition_value = [
            ['|', ('is_customer', '==', 'Nein'), ('is_supplier', '==', 'Nein'),
             ('language1', '==', 'German')], True,
            False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, False)
        # If is_customer==Ja and (is_supplier=Ja or language1==German)
        condition_value = [
            ['|', ('is_customer', '==', 'Ja'), ('is_supplier', '==', 'Ja'),
             ('language1', '==', 'German')], True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, True)
        # If is_customer==Ja and (is_supplier=Ja or language1==English)
        condition_value = [
            ['|', ('is_customer', '==', 'Ja'), ('is_supplier', '==', 'Ja'),
             ('language1', '==', 'English')], True,
            False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, False)

        # Check conditions with two fields
        condition_value = [[('language3', '==', '$language4')], True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, True)
        condition_value = [[('language2', '==', '$language4')], True, False]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, False)

        # Check conditions with char as true/false values
        condition_value = [[('language1', '==', 'German')], 'Hallo', 'Hello']
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, 'Hallo')
        condition_value = [[('language2', '==', 'German')], 'Hallo', 'Hello']
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, 'Hello')

        # Check conditions with int as true/false values
        condition_value = [[('language1', '==', 'German')], 1, 2]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, 1)
        condition_value = [[('language2', '==', 'German')], 1, 2]
        result = self.master_data_import._map_list_fields(line, 'res.partner',
                                                          'customer',
                                                          condition_value, {})
        self.assertEqual(result, 2)

    def test_140_map_m2x_fields(self):
        line = {
            'name': 'Test Partner',
            'category': self.partner_category1.id,
            'category_false': False,
            'category_name': self.partner_category1.name,
        }
        # Check usage of external identifier
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id', {
                                                             'extid':
                                                                 'category'
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [], 'values': {},
            'extid': self.partner_category1_extid_ref
        })

        # Check usage of external identifier when no value is set
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id', {
                                                             'extid':
                                                                 'category_false'
                                                         }, {})
        self.assertDictEqual(result, {'search': [], 'values': {}})

        # Check usage of search function
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'key': {
                                                                 'name':
                                                                     'category_name'
                                                             }
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [('name', '=', self.partner_category1.name)],
            'values': {}
        })

        # Check usage of search and key function where extid is stronger
        # then search
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'extid':
                                                                 'category',
                                                             'key': {
                                                                 'name':
                                                                     'category_name'
                                                             }
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [], 'values': {},
            'extid': self.partner_category1_extid_ref
        })

        # Check usage of values for create
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'mapping': {
                                                                 'name':
                                                                     'category_name'
                                                             }
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [], 'values': {'name': self.partner_category1.name}
        })

        # Check usage of values in combination with external identifier for
        # update
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'extid':
                                                                 'category',
                                                             'mapping': {
                                                                 'name':
                                                                     'category_name'
                                                             }
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [], 'values': {'name': self.partner_category1.name},
            'extid': self.partner_category1_extid_ref
        })

        # Check usage of values in combination with search for update
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'key': {
                                                                 'name':
                                                                     'category_name'
                                                             },
                                                             'mapping': {
                                                                 'name':
                                                                     'category_name'
                                                             }
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [('name', '=', self.partner_category1.name)],
            'values': {'name': self.partner_category1.name}
        })

        # Check usage of default values
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'default': {
                                                                 'name':
                                                                     'Test '
                                                                     'Default Name'
                                                             },
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [], 'values': {'name': 'Test Default Name'}
        })
        # Check usage of default values with overriden real value
        result = self.master_data_import._map_m2x_fields(line, 'res.partner',
                                                         'category_id',
                                                         {
                                                             'default': {
                                                                 'name':
                                                                     'Test '
                                                                     'Default Name'
                                                             },
                                                             'mapping': {
                                                                 'name':
                                                                     'category_name'
                                                             },
                                                         }, {})
        self.assertDictEqual(result, {
            'search': [], 'values': {'name': self.partner_category1.name}
        })

    def test_210_map_line(self):
        config = self.master_data_import_config
        config.update(
                {
                    'extid': 'number',
                    'mapping':
                        {
                            'ref': 'number',
                            'name': 'name',
                            'customer': 'customer',
                            'category_id': 'category',
                            'title': {
                                'key': {
                                    'name': 'title',
                                }
                            }
                        }
                })

        # Check line mapping
        line = {
            'number': 111,
            'name': 'Test Partner',
            'category': self.partner_category1.id,
            'category_name': self.partner_category1.name,
            'customer': 'X',
            'title': 'Mister',
        }
        result = self.master_data_import._map_line(line, config,
                                                   self.master_data_import_options)
        self.assertEqual(result,
                         {
                             'search': [],
                             'values': {
                                 'category_id': [
                                     (6, False, [self.partner_category1.id])],
                                 'ref': 111,
                                 'name': 'Test Partner',
                                 'customer': True,
                                 'title': {
                                     'search': [('name', '=', 'Mister')],
                                     'values': {}
                                 },
                             },
                             'extid': 'res_partner_111'
                         })

        # Check line mapping
        line = {
            'number': 111,
            'name': 'Test Partner',
            'category': self.partner_category1.id,
            'category_name': self.partner_category1.name,
            'customer': '',
            'title': 'Mister',
        }
        result = self.master_data_import._map_line(line, config,
                                                   self.master_data_import_options)
        self.assertEqual(result,
                         {
                             'search': [],
                             'values': {
                                 'category_id': [
                                     (6, False, [self.partner_category1.id])],
                                 'ref': 111,
                                 'name': 'Test Partner',
                                 'customer': False,
                                 'title': {
                                     'search': [('name', '=', 'Mister')],
                                     'values': {}
                                 },
                             },
                             'extid': 'res_partner_111'
                         })

    def test_220_read_chunk(self):
        config = self.master_data_import_config
        config.update(
                {
                    'extid': 'number',
                    'default': {
                        'is_company': True
                    },
                    'mapping':
                        {
                            'ref': 'number',
                            'name': 'name',
                            'customer': 'customer',
                            'category_id': 'category',
                            'parent_id': {
                                'extid': 'parent_id',
                            },
                            'child_ids': {
                                'key': {
                                    'name': 'contact_name'
                                },
                                'mapping': {
                                    'name': 'contact_name',
                                    'phone': 'contact_phone',
                                }
                            },
                        }
                })

        # Check creation of new record
        chunk_data = {
            'number': 111,
            'name': 'Test Partner',
            'customer': 'o',
            'category': self.partner_category1.id,
            'category_name': self.partner_category1.name,
            'contact_name': 'Sub Contact Partner',
            'contact_phone': '055 203 30 30',
            'parent_id': False,
        }
        self.master_data_import.read_chunk(config, chunk_data, False)
        new_partner = self.env['res.partner'].search([('ref', '=', 111)])
        self.assertEqual(len(new_partner), 1)
        self.assertEqual(new_partner.name, 'Test Partner')
        self.assertFalse(new_partner.customer)
        self.assertEqual(new_partner.is_company, True)
        self.assertEqual(len(new_partner.category_id), 1)
        self.assertEqual(new_partner.category_id[0], self.partner_category1)
        self.assertEqual(len(new_partner.child_ids), 1)
        self.assertEqual(new_partner.child_ids[0].name, 'Sub Contact Partner')

        # Now Update the created partner
        chunk_data = {
            'number': 111,
            'customer': 'x',
            'name': 'Test Partner Edited',
            'category': self.partner_category2.id,
            'category_name': self.partner_category2.name,
            'contact_name': False,
            'contact_phone': False,
            'parent_id': False,
        }
        self.master_data_import.read_chunk(config, chunk_data, False)
        result = self.env['res.partner'].search([('ref', '=', 111)])
        self.assertEqual(len(result), 1)
        self.assertEqual(result.name, 'Test Partner Edited')
        self.assertTrue(new_partner.customer)
        self.assertEqual(len(new_partner.category_id), 1)
        self.assertEqual(new_partner.category_id[0], self.partner_category2)
        self.assertEqual(len(new_partner.child_ids), 1)
        self.assertEqual(new_partner.child_ids[0].name, 'Sub Contact Partner')

        # Now Update the created partner while adding a second category and
        # adding second child
        chunk_data = {
            'number': 111,
            'name': False,
            'customer': False,
            'category': self.partner_category1_1.id,
            'category_name': self.partner_category1_1.name,
            'contact_name': 'Second Sub Contact Partner',
            'contact_phone': '123',
            'parent_id': False,
        }
        config['options']['m2m_mode'] = 'add'
        self.master_data_import.read_chunk(config, chunk_data, False)
        result = self.env['res.partner'].search([('ref', '=', 111)])
        self.assertEqual(len(result), 1)
        self.assertEqual(len(new_partner.category_id), 2)
        self.assertTrue(self.partner_category2 in new_partner.category_id)
        self.assertTrue(self.partner_category1_1 in new_partner.category_id)
        self.assertEqual(len(new_partner.child_ids), 2)
        self.assertTrue(
                any(p for p in new_partner.child_ids if
                    p.name == 'Second Sub Contact Partner' and p.phone ==
                    '123'))

        # Now Update the created partner while updating the second child
        # contact
        chunk_data = {
            'number': 111,
            'name': False,
            'customer': False,
            'category': False,
            'category_name': False,
            'contact_name': 'Second Sub Contact Partner',
            'contact_phone': '234',
            'parent_id': False,
        }
        config['options']['m2m_mode'] = 'add'
        self.master_data_import.read_chunk(config, chunk_data, False)
        result = self.env['res.partner'].search([('ref', '=', 111)])
        self.assertEqual(len(result), 1)
        self.assertEqual(len(new_partner.category_id), 2)
        self.assertTrue(self.partner_category2 in new_partner.category_id)
        self.assertTrue(self.partner_category1_1 in new_partner.category_id)
        self.assertEqual(len(new_partner.child_ids), 2)
        self.assertTrue(
                any(p for p in new_partner.child_ids if
                    p.name == 'Second Sub Contact Partner' and p.phone ==
                    '234'))

        # Now Add a sub partner to the created partner
        chunk_data = {
            'number': 1111,
            'name': 'Sub Partner',
            'customer': False,
            'category': False,
            'category_name': False,
            'contact_name': False,
            'contact_phone': False,
            'parent_id': 111,
        }
        self.master_data_import.read_chunk(config, chunk_data, False)
        result = self.env['res.partner'].search([('ref', '=', 1111)])
        self.assertEqual(len(result), 1)
        self.assertEqual(len(new_partner.category_id), 2)
        self.assertTrue(self.partner_category2 in new_partner.category_id)
        self.assertTrue(self.partner_category1_1 in new_partner.category_id)

        # Now Create a partner with join of existing title
        config['mapping']['title'] = {
            'key': {
                'name': 'title',
            }
        }
        chunk_data = {
            'number': 222,
            'customer': False,
            'name': 'Test Partner with title',
            'category': False,
            'category_name': False,
            'contact_name': False,
            'title': 'Mister',
            'contact_phone': False,
            'parent_id': False,
        }

        self.master_data_import.read_chunk(config, chunk_data, False)
        result = self.env['res.partner'].search([('ref', '=', 222)])
        self.assertEqual(len(result), 1)
        self.assertEqual(len(result.title), 1)
        self.assertEqual(result.title.name, 'Mister')

        config['mapping']['title']['mapping'] = {
            'name': 'title',
        }

        # Now Create a partner with search of not existing title
        chunk_data = {
            'number': 333,
            'name': 'Test Partner with new title',
            'title': 'Mister & Misses',
        }

        with self.assertRaises(ValueError):
            self.master_data_import.read_chunk(config, chunk_data, False)

    def test_221_read_chunk_invalid_field(self):
        config = self.master_data_import_config
        config2 = self.master_data_import_config
        config3 = self.master_data_import_config
        config4 = self.master_data_import_config
        chunk_data = {
            'name': 'Test Partner with new title',
        }
        # Space in odoo field
        config.update({
            'mapping': {
                'name ': 'name',
            }
        })
        # Space in odoo file field
        config2.update({
            'mapping': {
                'name': 'name ',
            }
        })
        # Upper letter in odoo field
        config3.update({
            'mapping': {
                'Name': 'name',
            }
        })
        # Upper letter in file field
        config4.update({
            'mapping': {
                'name': 'nAme',
            }
        })

        with self.assertRaises(ValueError):
            self.master_data_import.read_chunk(config, chunk_data, False)
        with self.assertRaises(ValueError):
            self.master_data_import.read_chunk(config2, chunk_data, False)
        with self.assertRaises(ValueError):
            self.master_data_import.read_chunk(config3, chunk_data, False)
        with self.assertRaises(ValueError):
            self.master_data_import.read_chunk(config4, chunk_data, False)

    def test_222_read_chunk_many2many_extid(self):
        config = self.master_data_import_config
        config.update({
            'mapping':
                {
                    'ref': 'number',
                    'name': 'name',
                    'category_id': {
                        'extid': 'category',
                    },
                }
        })
        chunk_data = {
            'name': 'Test Partner with title',
            'category': str(self.partner_category1.id),
            'number': 111
        }
        # self.master_data_import.read_chunk(config_category, chunk_data, False)
        self.master_data_import.read_chunk(config, chunk_data, False)
        partner = self.env['res.partner'].search([('ref', '=', 111)])
        self.assertEqual(len(partner), 1)
        self.assertEqual(len(partner.category_id), 1)
        self.assertEqual(partner.category_id, self.partner_category1)

    def setUp(self):
        super(TestMasterDataImport, self).setUp()
        self.master_data_task = self.env['impexp.task'].create({
            'name': 'Test Master Data Import',
            'task': 'master_data_import',
            'config': """{
    'model': 'res.partner',
    'options': {
        'date_format': '%d.%m.%Y',
        'time_format': '%H:%M',
        'm2m_mode': 'replace',
    }
}"""
        })
        self.master_data_import = self.master_data_task.get_task_instance()
        self.master_data_import_config = self.master_data_task._config()
        self.master_data_import_options = self.master_data_import_config.get(
                'options')

        self.sub_partner = self.env['res.partner'].create({
            'name': 'Test Contact',
        })

        self.company_partner = self.env['res.partner'].create({
            'name': 'Test',
            'is_company': True,
            'child_ids': [(0, False, {'name': 'Test Subcontact'}), ]
        })

        self.partner_category1 = self.env['res.partner.category'].create({
            'name': 'Category 1',
        })
        self.partner_category1_extid_ref = 'res_partner_category_' + str(
                self.partner_category1.id)
        self.partner_category1_extid = self.env['ir.model.data'].create({
            'model': 'res.partner.category',
            'module': '__export__',
            'name': self.partner_category1_extid_ref,
            'res_id': self.partner_category1.id
        })
        self.partner_category1_1 = self.env['res.partner.category'].create({
            'name': 'Category 1.1',
            'parent_id': self.partner_category1.id,
        })
        self.partner_category2 = self.env['res.partner.category'].create({
            'name': 'Category 2',
        })
